<?php
	$Websites = [
		[
			"Title" => "MineAlert",
			"PageName" => "minealert",
			"Desc" => "MineAlert is a service that will email you when your Minecraft server is offline; this could be due to Network issues on your end or a server crash - MineAlert will notify you if this were to happen.<br><br><b>This website is closed down.</b>",
			"MoreInfo" => "portfolio/website/",
			"ToolsUsed" => "PHP,MySQL,CSS,Bootstrap 3,HTML",
			"Status" => "Discontinued",
			"BitBucketLink" => "none",
			"URL" => "http://minealert.wmportfolio.co.uk/"
		],
		[
			"Title" => "The Crafters Network (Statistics Site)",
			"PageName" => "thecraftersstats",
			"Desc" => "The statistics website is being created to show information about a player. The website connects to a MySQL database which pulls information out, then displays the information on the page. This is still being worked on while I also create a Java Plugin to handle the player loggin inside the game.",
			"MoreInfo" => "portfolio/website/",
			"ToolsUsed" => "PHP,MySQL,CSS,Bootstrap 3,jQuery,HTML",
			"Status" => "In Development",
			"BitBucketLink" => "https://bitbucket.org/Darkcop/portfolio-statistics-site",
			"URL" => "https://thecrafters.net/stats/"
		],
		[
			"Title" => "The Crafters Network (Portal)",
			"PageName" => "thecrafterslanding",
			"Desc" => "Shows links on the website that are static on the page, it also querys a ip address to check if it's up or not.",
			"MoreInfo" => "portfolio/website/",
			"ToolsUsed" => "PHP,CSS,HTML,jQuery",
			"Status" => "Active",
			"BitBucketLink" => "https://bitbucket.org/Darkcop/portfolio-portal-site",
			"URL" => "https://thecrafters.net/"
		],
		[
			"Title" => "Gaming Servers",
			"PageName" => "gamingservers",
			"Desc" => "Website for clients to buy game servers from the company, which are rented out to the client.",
			"MoreInfo" => "portfolio/website/",
			"ToolsUsed" => "PHP,MySQL,CSS,Bootstrap 3,HTML,jQuery",
			"Status" => "Active",
			"BitBucketLink" => "none",
			"URL" => "http://gamingservers.us/"
		],
		[
			"Title" => "WMPortfolio",
			"PageName" => "wmportfolio",
			"Desc" => "Website to show my portfolio work.",
			"MoreInfo" => "portfolio/website/",
			"ToolsUsed" => "PHP,CSS,Bootstrap 3,HTML,jQuery",
			"Status" => "Active",
			"BitBucketLink" => "https://bitbucket.org/Darkcop/portfolio-wmportfolio-site",
			"URL" => "http://wmportfolio.co.uk/"
		]
	];

	$ServerManagement = [
		[
			"Title" => "Gaming Servers",
			"PageName" => "gamingservers",
			"Desc" => "I volunteer to help run and manage debian based servers that are used to run game servers also websites on.",
			"MoreInfo" => "portfolio/management/",
			"ToolsUsed" => "Debian,Linux",
			"URL" => "http://gamingservers.us/"
		],
		[
			"Title" => "The Crafters Network",
			"PageName" => "thecraftersserver",
			"Desc" => "I volunteer to help run and manage debian based servers that are used to run game servers.",
			"MoreInfo" => "portfolio/management/",
			"ToolsUsed" => "Debian,Linux",
			"URL" => "https://thecrafters.net/"
		]
	];

	$PluginAddons = [
		[
			"Title" => "AutoLockThead",
			"PageName" => "autolockthead",
			"Desc" => "...",
			"MoreInfo" => "portfolio/pluginaddons/",
			"URL" => "#"
		]
	];
?>