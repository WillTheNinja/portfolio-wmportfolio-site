<?php include'Config.php';
$sentInfo = strtolower($_REQUEST['info']);

foreach ($Websites as $Website): 
if($sentInfo == strtolower($Website['PageName'])):
?>
<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no">
    <title>Websites - WMPorfolio</title>
    <link rel="stylesheet" type="text/css" href="../../assets/css/bootstrap.css" />
    <link rel="stylesheet" type="text/css" href="../../assets/css/custom.css" />
    <link rel="stylesheet" type="text/css" href="../../assets/css/normalize.css" />
    <link rel="stylesheet" type="text/css" href="../../assets/css/Animate.css" />

    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
      <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
  </head>

  <body>

    <nav class="navbar navbar-inverse navbar-fixed-top">
      <div class="container-fluid">
        <div class="navbar-header">
          <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar" aria-expanded="false" aria-controls="navbar">
            <span class="sr-only">Toggle navigation</span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
          </button>
          <a class="navbar-brand" href="#">Will's Portfolio</a>
        </div>
      </div>
    </nav>

    <div class="container-fluid">
      <div class="row">
        <div class="col-sm-3 col-md-2 sidebar">
          <ul class="nav nav-sidebar">
            <!--<li><a href="../../index#overview">Overview</a></li>-->
            <li><a href="../../index#websites">Websites</a></li>
            <li><a href="../../index#servermanagement">Server Management</a></li>
          </ul>
        </div>
        <div class="col-sm-9 col-sm-offset-3 col-md-10 col-md-offset-2 main">

          <h2 id="overview" class="sub-header contentTitle">Website: <?php echo $Website['Title'];?></h2>
          <div class="panel panel-default">
            <div class="panel-heading">
              <span class="panel-title">Description <span class="label label-info pull-right">Status: <?php echo $Website['Status'];?></span></span>
            </div>
            <div class="panel-body">
              <?php echo $Website['Desc'];?>
            </div>
          </div>
          <div class="panel panel-default">
            <div class="panel-body"><p>Tools Used: 
              <?php $split = explode(",", $Website['ToolsUsed']); foreach($split as $tags): ?>
                <span class="label label-primary"><?php echo $tags; ?></span>
              <? endforeach;?></p>
              <p>Website URL: <a target="_blank" href="<?php echo $Website['URL']; ?>"><?php echo $Website['URL']; ?></a></p>
              <?php if($Website['BitBucketLink'] != "none"): ?>
                <p>Source Code: <a target="_blank" href="<?php echo $Website['BitBucketLink']; ?>"><?php echo $Website['BitBucketLink']; ?></a></p>
              <?php endif; ?>
            </div>
          </div>
        </div>
      </div>
    </div>
  </body>
  <script type='text/javascript' src="../../assets/js/jquery-1.11.3.min.js"></script>
  <script type='text/javascript' src="../../assets/js/bootstrap.js"></script>
  <script type='text/javascript' src="../../assets/js/custom.js"></script>
  <script type='text/javascript' src="../../assets/js/lettering.js"></script>
  <script type='text/javascript' src="../../assets/js/textillate.js"></script>
</html>
<?php 
endif;
endforeach;
?>