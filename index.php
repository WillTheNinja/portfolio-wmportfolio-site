<?php include'Config.php'; ?>
<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no">
    <title>Home - WMPorfolio</title>
    <link rel="stylesheet" type="text/css" href="assets/css/bootstrap.css" />
    <link rel="stylesheet" type="text/css" href="assets/css/custom.css" />
    <link rel="stylesheet" type="text/css" href="assets/css/normalize.css" />
    <link rel="stylesheet" type="text/css" href="assets/css/Animate.css" />

    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
      <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
  </head>

  <body>

    <nav class="navbar navbar-inverse navbar-fixed-top">
      <div class="container-fluid">
        <div class="navbar-header">
          <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar" aria-expanded="false" aria-controls="navbar">
            <span class="sr-only">Toggle navigation</span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
          </button>
          <a class="navbar-brand" href="#">Will's Portfolio</a>
        </div>
      </div>
    </nav>

    <div class="container-fluid">
      <div class="row">
        <div class="col-sm-3 col-md-2 sidebar">
          <ul class="nav nav-sidebar">
            <!--<li class="active"><a href="#overview">Overview</a></li>-->
            <li><a href="#websites">Websites</a></li>
            <li><a href="#servermanagement">Server Management</a></li>
          </ul>
        </div>
        <div class="col-sm-9 col-sm-offset-3 col-md-10 col-md-offset-2 main">

          <!--<h2 id="overview" class="sub-header contentTitle">About Me</h2>
          <div class="panel panel-default">
            <div class="panel-body">
              <p class="aboutMe"></p>
            </div>
          </div>-->

          <h2 id="websites" class="sub-header contentTitle">Websites</h2>
            <div class="row">
              <?php foreach ($Websites as $Website):  ?>
              <div class="col-sm-6 col-md-4">
                <div class="thumbnail">
                  <div class="caption">
                    <h3><?php echo $Website['Title']; ?></h3>
                    <!--<p><?php echo $Website['Desc']; ?></p>-->
                    <p><a href="<?php echo $Website['MoreInfo'] . $Website['PageName']; ?>" class="btn btn-primary" role="button">More Information</a> <a target="_blank" href="<?php echo $Website['URL']; ?>" class="btn btn-default" role="button">Visit Website</a></p>
                  </div>
                </div>
              </div>
              <?php endforeach; ?>
            </div>

          <h2 id="servermanagement" class="sub-header contentTitle">Server Management</h2>
            <div class="row">
              <?php foreach ($ServerManagement as $SM):  ?>
              <div class="col-sm-6 col-md-4">
                <div class="thumbnail">
                  <div class="caption">
                    <h3><?php echo $SM['Title']; ?></h3>
                    <!--<p><?php echo $SM['Desc']; ?></p>-->
                    <p><a href="<?php echo $SM['MoreInfo'] . $SM['PageName']; ?>" class="btn btn-primary" role="button">More Information</a> <a target="_blank" href="<?php echo $SM['URL']; ?>" class="btn btn-default" role="button">Visit Website</a></p>
                  </div>
                </div>
              </div>
              <?php endforeach; ?>
            </div>
        </div>
      </div>
    </div>
  </body>
  <script type='text/javascript' src="assets/js/jquery-1.11.3.min.js"></script>
  <script type='text/javascript' src="assets/js/bootstrap.js"></script>
  <script type='text/javascript' src="assets/js/custom.js"></script>
  <script type='text/javascript' src="assets/js/lettering.js"></script>
  <script type='text/javascript' src="assets/js/textillate.js"></script>
  <script type="text/javascript">
  $(function () {
    $('.aboutMe').textillate();
  })
  </script>
</html>
